# Description
Ubuntu-based image file used to compile stuff for ROS. It is recommended to pull the Docker image `gscar/ros`, but one may also build running the provided build script.

# Pull from Repository
```
docker pull gscar/ros:<tag>
```
Available tags are: `melodic` (that's it for now).
# Build
It is recommended to pull the image using the previous steps. Alternatively, if you wish to build the image yourself, please use the provided build script.
```
$ git clone https://gitlab.com/gscar-coppe-ufrj/docker/ros gscar-ros
$ cd gscar-ros
$ chmod +x build
$ ./build <tag>
```
This script will generate the Docker image and name it as `gscar/ros:<tag>`.

# Run
To run the container, we recommend using the provided `docker-ros` script. For that, give run permission through `chmod +x docker-ros` and place the script somewhere in your path.
```
$ docker-ros [your command goes here]
```
The script mounts the current directory on the container's `/data` directory. If you run the script from you catkin workspace directory, it will automatically source `devel/setup.bash`.

# License
This Software is distributed under the [MIT License](https://opensource.org/licenses/MIT).
