#!/bin/bash
set -e

fname="/data/devel/setup.bash"

source /opt/ros/$ROS_DISTRO/setup.bash
if [ -f "$fname" ] ; then
    source "$fname"
fi

exec "$@"
