FROM ros:melodic-robot

ARG USER_ID="1000"
ARG GROUP_ID="1000"
ARG USER_NAME="ros"
ARG GROUP_NAME="ros"

# Install ros packages
RUN apt-get update && apt-get install -y \
    ros-melodic-desktop-full \
    python-catkin-tools \
    && rm -rf /var/lib/apt/lists/*

# Add user
RUN groupadd -g ${GROUP_ID} ${GROUP_NAME} && \
    useradd -l -u ${USER_ID} -g ${GROUP_NAME} ${USER_NAME} && \
    install -d -m 0755 -o ${USER_NAME} -g ${GROUP_NAME} /home/${USER_NAME}

# Create data mountpoint
RUN mkdir /data
RUN chown -R ${USER_NAME}:${GROUP_NAME} /data
VOLUME ["/data"]

# Change user
USER ${USER_NAME}

# Setup ROS environment variables used in the entrypoint
ENV ROS_DISTRO "melodic"

# Entrypoint
WORKDIR "/home/${USER_NAME}"
RUN mkdir -p ".local/bin"
ENV PATH "$PATH:/home/${USER_NAME}/.local/bin"
COPY --chown=${USER_NAME}:${GROUP_NAME} "entrypoint.sh" ".local/bin"
RUN chmod +x ".local/bin/entrypoint.sh"
ENTRYPOINT ["entrypoint.sh"]
CMD ["bash"]

# Change working directory
WORKDIR /data